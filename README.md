# RoiOverStack

## Pipeline
* segment ROIs based on user defined stack ID
* calculate average internsity per ROI
* print a table stack id / roi id

## GUI Pipeline
1. File -> Open...
2. Image -> Duplicate... -> Channels (c): 10 -> OK
3. Process -> Smooth
4. Image -> Adjust -> Threshold... -> Li + Red + Dark background -> Apply
5. Analyze -> Analyze Particles... -> Size 50-Infinity + Pixel units + Show: Overlay Masks + Add to Manager + Exclude on edges -> OK

## ROI detection
* depends on the Threshold [step](https://gitlab.mpcdf.mpg.de/mpibr/scic/roioverstack/-/blob/main/RoiOverStack.ijm#L43): explore different automatic threshold algorithms
* depends on [size](https://gitlab.mpcdf.mpg.de/mpibr/scic/roioverstack/-/blob/main/RoiOverStack.ijm#L44) in particle detection: minimum is set on 50 sq. pixels

## Output
Two outputs with the same name and path as the image are exported:
* all detected ROIs in a `.zip`
* all measurements in a `.csv`