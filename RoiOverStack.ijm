/*
 * RoiOverStack macro
 *     segment ROIs based on given stack ID
 *     calculate measures per ROI
 *     calculate distance to user defined line
 * version 0.2
 * Sep 2022
 * sciclist@brain.mpg.de
 */



macro "RoiOverStack"
{
    // set defaults
    default_path = File.getDefaultDir;
    default_channel_a = 11;
    default_channel_b = 20;

    // create an input dialog
    Dialog.create("RoiOverStack Arguments");
    Dialog.addFile("image:", default_path);
    Dialog.addFile("roi:", default_path);
    Dialog.addNumber("channel A:", default_channel_a);
    Dialog.addNumber("channel B:", default_channel_b);
    Dialog.show();

    // parse arguments
    file_image = Dialog.getString();
    file_roi = Dialog.getString();
    channel_a = Dialog.getNumber();
    channel_b = Dialog.getNumber();    

    out_path = getFilePath(file_image);
    out_name = getFileName(file_image);
    out_name = File.getNameWithoutExtension(out_name);
    //out_name_roi = getFileName(file_roi, "roi");
    file_input_roi = roiFileGiven(file_roi, "roi");    
    
    // pipeline
    run("Clear Results");
    roiManager("Reset");
    
    // pipeline
    run("Bio-Formats Windowless Importer", "open=" + file_image);
    window_title_stack = getTitle();
    
    setBatchMode(true);

    list_rois_a = createRois(window_title_stack, channel_a);
    list_rois_b = createRois(window_title_stack, channel_b);
    
    idx_union_a = createUnionMask(list_rois_a, channel_a);
    idx_union_b = createUnionMask(list_rois_b, channel_b);
    
    list_overlays_a = findOverlayRois(idx_union_b, list_rois_a);
    list_overlays_b = findOverlayRois(idx_union_a, list_rois_b);

    deleteListOfRois(newArray(idx_union_a, idx_union_b));

    measureStackProperties(window_title_stack, out_path, out_name);
    measureDistanceToBorder(file_roi, out_path, out_name, file_input_roi);
    
    setBatchMode(false);
}



/* --- FUNCTIONS --- */



function quitWithError(message)
{
    if (!endsWith(message, "."))
        message = message + ".";
    exit("RoiOverStack:error, " + message);
}



function roiFileGiven(file, extension)
{
    if (File.isFile(file) && 
        File.exists(file) &&
        endsWith(file, extension)) {
        return 1;
    }
    else {
        return 0;
    }
}


function getFilePath(file)
{
    path_file = File.getDirectory(file);
    if (File.isDirectory(path_file))
        return path_file;
    else
        quitWithError("file path does not exist or is not writable");
}



function getFileName(file)
{
    if (File.isFile(file) && 
        File.exists(file) &&
        (file.endsWith("czi") || 
        file.endsWith("CZI") ||
        file.endsWith("tiff") ||
        file.endsWith("TIFF") ||
        file.endsWith("tif") ||
        file.endsWith("TIF"))) {
        return File.getName(file);
    }
    else {
        quitWithError("provide valid czi or tiff file");
    }
}



function getLastRoiIndex()
{
    return roiManager("Count") - 1;
}



function deleteListOfRois(list_rois)
{
    roiManager("Select", list_rois);
    roiManager("Delete");
}



function loadBorderRoi(file_roi)
{
    roiManager("open", file_roi);
    idx_roi_border = getLastRoiIndex();
    if (idx_roi_border < 0)
        quitWithError("border ROI not loaded in manager");
    return idx_roi_border;
}



function createRois(window_title_stack, stack_id)
{
    idx_roi_start = roiManager("Count");
    selectWindow(window_title_stack);
    //run("Duplicate...", "duplicate channels=" + stack_id);
    run("Duplicate...", "duplicate range=" + stack_id + "-" + stack_id);
    getTitle();
    run("Smooth");
    setAutoThreshold("Li dark");
    run("Analyze Particles...", "size=50-Infinity show=[Overlay Masks] add");
    idx_roi_stop = roiManager("Count");

    if (idx_roi_start == idx_roi_stop)
        quitWithError("no ROIs were detected on channel " + stack_id);

    span_rois = idx_roi_stop - idx_roi_start;
    list_rois = newArray(span_rois);
    for (i = 0; i < span_rois; i++) {
        idx_roi = idx_roi_start + i;
        list_rois[i] = idx_roi;
        roiManager("Select", idx_roi);
        name_old = RoiManager.getName(idx_roi);
        name_new = "" + stack_id + "-" + name_old;
        roiManager("Rename", name_new);
    }
    return list_rois;
}



function createUnionMask(list_rois, stack_id)
{
    roiManager("Select", list_rois);
    roiManager("Combine");
    roiManager("Add");
    idx_mask = roiManager("Count") - 1;
    roiManager("Select", idx_mask);
    name = "" + stack_id + "-union-" + RoiManager.getName(idx_mask);
    roiManager("Rename", name);
    roiManager("Deselect");
    return idx_mask;
}



function findOverlayRois(idx_overlay_mask, list_rois)
{
    list_overlays = newArray();
    list_xors = newArray();

    for (i = 0; i < list_rois.length; i++) {
        idx_roi = list_rois[i];
        name = RoiManager.getName(idx_roi);

        // create xor mask
        roiManager("Select", newArray(idx_overlay_mask, idx_roi));
        roiManager("XOR");
        roiManager("Add");
        idx_xor_mask = getLastRoiIndex();
        roiManager("Select", idx_xor_mask);
        roiManager("Rename", name + "-xor");
        list_xors = Array.concat(list_xors, idx_xor_mask);

        // find unique
        roiManager("Select", newArray(idx_overlay_mask, idx_roi));
        roiManager("AND");
        if (selectionType() == -1) {
            roiManager("Select", idx_roi);
            roiManager("Rename", name + "-unique");
        }
        
        // find overlays
        roiManager("Select", newArray(idx_xor_mask, idx_roi));
        roiManager("AND");
        if (selectionType() > -1) {
            roiManager("Add");
            idx_overlay = getLastRoiIndex();
            roiManager("Select", idx_overlay);
            roiManager("Rename", name + "-overlay");
            list_overlays = Array.concat(list_overlays, idx_overlay);
        }

    }
    roiManager("Deselect");
    deleteListOfRois(list_xors);

    return list_overlays;
}


function checkRoiBorderCoordinates_x(file_roi, img_width)
{
    idx_roi_border = roiManager("count");
    roiManager("Open", file_roi);
    roiManager("Select", idx_roi_border);
    Roi.getCoordinates(points_x, points_y);
    for (i=0; i<points_x.length; i++) {
            if (points_x[i] < 0) {
                points_x[i] = 0;
            }
            else if (points_x[i] > img_width) {
                points_x[i] = img_width;
            }
        }
    return points_x; 
}

function checkRoiBorderCoordinates_y(file_roi, img_width)
{
    idx_roi_border = roiManager("count");
    roiManager("Select", idx_roi_border-1);
    Roi.getCoordinates(points_x, points_y);
    for (i=0; i<points_x.length; i++) {
            if (points_y[i] < 0) {
                points_y[i] = 0;
            }
            else if (points_y[i] > img_width) {
                points_y[i] = img_width;
            }
    }
    return points_y; 
}


function measureDistanceToBorder(file_roi, out_path, out_name, file_input_roi)
{
    if(file_input_roi){
        img_width = getWidth();
        points_x = checkRoiBorderCoordinates_x(file_roi, img_width);
        points_y = checkRoiBorderCoordinates_y(file_roi, img_width);
        //setOption("Show All", true);
        roiManager("Delete");
        n_edges = points_x.length - 1;
        list_dx = newArray(n_edges);
        list_dy = newArray(n_edges);
        list_norm = newArray(n_edges);
        for(i = 0; i < n_edges; i++) {
            value_dx = points_x[i + 1] - points_x[i];
            value_dy = points_y[i + 1] - points_y[i];
            value_norm = Math.sqrt(Math.sqr(value_dx) + Math.sqr(value_dy));
            list_dx[i] = value_dx;
            list_dy[i] = value_dy;
            list_norm[i] = value_norm;
        }
        // calculate distance per roi
        out_file = out_path + File.separator + out_name + "_distance.csv";
        fh = File.open(out_file);
        n_rois = roiManager("Count");
        print(fh, "roi.index,roi.name,roi.x.center.pixel,roi.y.center.pixel,roi.distance.pixel,roi.distance.micron");
        getPixelSize(unit, pixelWidth, pixelHeight);
        for (i = 0; i < n_rois; i++) {
            roiManager("Select", i);
            Roi.getCoordinates(points_roi_x, points_roi_y);
            center_roi_x = arrayMean(points_roi_x);
            center_roi_y = arrayMean(points_roi_y);
            euclidean_dist = getClosestDistance(center_roi_x, center_roi_y, points_x, points_y, list_norm);
            euclidean_dist_mm = euclidean_dist * pixelWidth;
            print(fh, i + "," + Roi.getName + "," +
                d2s(center_roi_x, 4) + "," +
                d2s(center_roi_y, 4) + "," + 
                d2s(euclidean_dist, 4) + "," + 
                d2s(euclidean_dist_mm, 4));
    
        }
    }
    else
         setOption("Show All", true);
}



function arrayMean(values)
{
    n = values.length;
    s = 0;
    for (i = 0; i < n; i++)
        s += values[i];
    return s/n;
}


function norm_c(v)
{
    v_sum = 0;
    for (i=0; i < v.length; i++) {
        v_sum = v_sum + v[i] * v[i];
    }
    n = Math.sqrt(v_sum);
    return n;
}


function dot_prod(va, vb)
{
    dp = 0;
    if (va.length == vb.length) {
        for (i=0; i < va.length; i++) {
            dp = dp + va[i] * vb[i];
        }
    }
    return dp;
}


function point_line_dist(point_x, point_y, lineStart_x, lineStart_y, lineEnd_x, lineEnd_y)
{
    lineDir_x = lineEnd_x - lineStart_x;
    lineDir_y = lineEnd_y - lineStart_y;
    lineDir = Array.concat(lineDir_x,lineDir_y);
    lineLength = norm_c(lineDir);
    for (i=0; i < lineDir.length; i++) {
        lineDir[i] = lineDir[i] / lineLength;
    }
    pointDir_x  = point_x - lineStart_x;
    pointDir_y  = point_y - lineStart_y;
    pointDir = Array.concat(pointDir_x,pointDir_y);
    pointDist =  dot_prod(pointDir, lineDir);
    closestPoint_x = lineStart_x + pointDist * lineDir_x;
    closestPoint_y = lineStart_y + pointDist * lineDir_y;
    closestPoint = norm_c(Array.concat(closestPoint_x,closestPoint_y));
    if (pointDist < 0) {
        pdx = point_x - lineStart_x;
        pdy = point_y - lineStart_y;
        dist = norm_c(Array.concat(pdx,pdy));
    }
    else if (pointDist > lineLength) {
        pdx = point_x - lineEnd_x;
        pdy = point_y - lineEnd_y;
        dist = norm_c(Array.concat(pdx,pdy));
    }
    else {
        pdx = point_x - closestPoint_x;
        pdy = point_y - closestPoint_y;
        dist = norm_c(Array.concat(pdx,pdy));
    }
    return dist;
}


function getClosestDistance(center_roi_x, center_roi_y,
                            points_x, points_y, list_norm)
{
    n_edges = list_norm.length;
    dist_max = 0;
    for (i = 0; i < n_edges; i++) {
        dist = point_line_dist(center_roi_x, center_roi_y, points_x[i], points_y[i], points_x[i+1], points_y[i+1]);
        if (i == 0)
            dist_max = dist;
        if ((0 < i)  && (dist < dist_max))
            dist_max = dist;
    }
    return dist_max;
}



function measureStackProperties(window_title_stack, out_path, out_name)
{
    // measure stack
    selectWindow(window_title_stack);
    pixel_width = Property.getNumber("Scaling|Distance|Value #1"); // pixel width is reported in meters
    pixel_distance = 1 / (pixel_width * 1e6); // convert to microns, and flip to microns/pixel
    run("Set Scale...", "distance=" + pixel_distance + " known=1 unit=micron");
    run("Set Measurements...", "display labels area mean min center stack decimal=4");
    roiManager("multi-measure measure_all");

    // re-lable name
    for (i = 0; i < nResults; i++) {
        label_old = getResultLabel(i);
        idx_start = indexOf(label_old, ":", 0) + 1;
        idx_end = indexOf(label_old, ":", idx_start);
        label_new = substring(label_old, idx_start, idx_end);
        setResult("Label", i, label_new);
    }

    // save results
    out_file = out_path + File.separator + out_name;
    roiManager("save", out_file + "_rois.zip");
    selectWindow("Results");
    saveAs("Measurements", out_file + "_properties.csv");
}
