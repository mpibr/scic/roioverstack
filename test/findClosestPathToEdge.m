%% findShortestPathToEdge
clc
clear variables
close all

%% generate random coordinates
npixels = 1024;
xy = randi(npixels, [1, 2]);

nnodes = 12;
[px, py] = simple_polygon(nnodes);
px = px * npixels;
py = py * npixels;

%% test straight line
%px = [100;1024];
%py = [100;100];

%% calculate closest point and distance
nedges = size(px,1) - 1;
dist = zeros(nedges, 1);
pts = zeros(nedges, 2);
for e = 1 : nedges
    [d, p] = point_line_distance(xy, [px(e), py(e)], [px(e+1), py(e+1)]);
    dist(e) = d;
    pts(e,:) = p;
end

[dist_min, idx_min] = min(dist);
disp(dist_min);
xy_proj = [xy;pts(idx_min,:)];
%}

figure('color', 'w');
plot(px,py, 'r');
hold on;
plot(px, py, 'r.','markersize', 5);
plot(xy(:,1), xy(:,2), 'k.');
plot(xy_proj(:,1), xy_proj(:,2), '-','color',[.65,.65,.65]);
plot(xy_proj(2,1), xy_proj(2,2), 'k.', 'markersize', 5);
hold off;


%% functions
function [dist, closestPoint] = point_line_distance(point, lineStart, lineEnd)
    % Helper function to find the distance between a point and a line segment
    % point is a 1x2 array representing the (x,y) coordinates of the point
    % lineStart and lineEnd are 1x2 arrays representing the (x,y) coordinates of the endpoints of the line segment
    
    % Project the point onto the line defined by lineStart and lineEnd
    lineDir = lineEnd - lineStart;
    %lineLength = norm(lineDir);
    lineLength = norm_c(lineDir);
    lineDir = lineDir/lineLength; % normalize the line direction
    pointDir = point - lineStart;
    %pointDist = dot(pointDir, lineDir);
    pointDist = dot_prod(pointDir, lineDir);
    closestPoint = lineStart + pointDist*lineDir;
    
    % Check if the projection lies on the line segment
    if pointDist < 0
        % Point is before the start of the line segment
        %dist = norm(point - lineStart);
        dist = norm_c(point - lineStart);
        closestPoint = lineStart;
    elseif pointDist > lineLength
        % Point is past the end of the line segment
        %dist = norm(point - lineEnd);
        dist = norm_c(point - lineEnd);
        closestPoint = lineEnd;
    else
        % Point is on the line segment
        %dist = norm(point - closestPoint);
        dist = norm_c(point - closestPoint);
    end

end


function n = norm_c(v)
    v_sum = 0;
    for i = 1 : length(v)
        v_sum = v_sum + v(i) * v(i);
    end
    n = sqrt(v_sum);
end


function dp = dot_prod(va, vb)
    dp = 0;
    if length(va) == length(vb)
        for i = 1 : length(va)
            dp = dp + va(i) * vb(i);
        end
    end
end

function [x, y] = simple_polygon(numSides)

    % Define the radius of the polygon's circumscribed circle
    radius = rand(1);

    % Generate random angles for each vertex of the polygon
    angles = sort(rand(numSides, 1))*2*pi;

    % Convert the polar coordinates to Cartesian coordinates
    x = radius*cos(angles);
    y = radius*sin(angles);

    % Shift the polygon to a random position
    x = x + rand(1);
    y = y + rand(1);

    % normalize between 0,1
    x = (x - min(x))./(max(x) - min(x));
    y = (y - min(y))./(max(y) - min(y));
    
end

